package task5;

class Doll {
    int id;
    int qualityScoreMachine;
    boolean imperfect, isPainted;

    public Doll(int id, int qualityScoreMachine) {
        this.id = id;
        this.qualityScoreMachine = qualityScoreMachine;
    }

    public void setPainted(boolean painted) {
        isPainted = painted;

    }

    public int getQualityScore() {
        return qualityScoreMachine;
    }

    public void hasImperfections(boolean imperfect) {
        this.imperfect = imperfect;

    }
}