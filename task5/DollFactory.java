package task5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

class DollFactory {
    List<Doll> dolls;
    private CyclicBarrier stageA, stageB, stageC;

    private void execution(int dollsNumber) throws InterruptedException {
        stageA = new CyclicBarrier(dollsNumber);
        stageB = new CyclicBarrier(dollsNumber);
        stageC = new CyclicBarrier(dollsNumber);
        dolls = new ArrayList<>(dollsNumber);
        for (int i = 0; i < dollsNumber; i++) {
            Process task = new Process(i);
            Thread t = new Thread(task);
            t.start();
        }
        try {
            stageC.await();
            System.out.println("Packaging process D"); // TODO print results
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        DollFactory dcb = new DollFactory();

        dcb.execution(11);
    }

    class Process implements Runnable {
        int id;

        public Process(int id) {
            this.id = id;
        }

        public void run() {
            Doll d = assembly();
            try {
                stageA.await();
                painting(d);
                stageB.await();
                qualityControl(d);
            } catch (InterruptedException | BrokenBarrierException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        void painting(Doll d) {
            d.setPainted(true);
        }

        Doll assembly() {
            Random r = new Random();
            return new Doll(id, r.nextInt(4) + 7);
        }

        void qualityControl(Doll d) {
            if (d.getQualityScore() >= 9) {
                d.hasImperfections(false);
                dolls.add(d);
            }
        }
    }
}