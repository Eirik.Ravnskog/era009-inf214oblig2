package task4;

public class Printer {

    private Sem sem;

    public Printer() {
        sem = new Sem(1);
    }

    public static void main(String[] args) throws InterruptedException {
        Printer p = new Printer();
        Thread p1 = new Thread(p.new P(1));
        Thread p2 = new Thread(p.new P(3));
        Thread p3 = new Thread(p.new P(5));
        p1.start();
        p2.start();
        p3.start();
        p1.join();
        p2.join();
        p3.join();
    }

    class P implements Runnable {
        private int i;

        public P(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    sem.acquire();
                    System.out.println(i);
                    Thread.sleep(100);
                    System.out.println(i + 1);
                    Thread.sleep(100);
                    sem.release();
                    break;
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }

        }

    }
}