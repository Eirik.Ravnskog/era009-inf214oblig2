package task4;

public class Sem {
    private int permits;

    public Sem(int permits) {
        this.permits = permits;
    }

    public synchronized void acquire() throws InterruptedException {
        if (permits > 0) {
            this.permits--;
        } else {
            throw new InterruptedException("Not enough resource");
        }
    }

    public synchronized void release() {
        this.permits++;
    }
}