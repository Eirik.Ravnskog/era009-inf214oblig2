package task1;

import java.util.Random;

public class Test {
    public static double[][] generate(int rows, int columns) {
        double[][] ret = new double[rows][columns];
        Random random = new Random();
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                ret[i][j] = random.nextDouble() * 10;
        return ret;
    }

    public static void main(String[] args) {
        double A[][] = generate(1000, 1000);
        double B[][] = generate(1000, 1000);
        double C[][] = new double[1000][1000];

        long start = System.currentTimeMillis();
        SerialMult.multiply(A, B, C);
        long end = System.currentTimeMillis();
        System.out.println(String.format("%d", end - start));

        start = System.currentTimeMillis();
        MatrixMult.multiply(A, B, C);
        end = System.currentTimeMillis();
        System.out.println(String.format("%d", end - start));

    }
}